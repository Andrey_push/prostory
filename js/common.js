jQuery(document).ready(function($){
	//$('.fancybox').fancybox();

	$("form").submit(function() { //Change
		var th = $(this);
		$.ajax({
			type: "POST",
			url: "mail.php", //Change
			data: th.serialize()
		}).done(function() {
			setTimeout(function() {
				alert("Ваша заявка принята!")
			}, 1000);
		});
		return false;
	});

	//Убираем placeholder при клике и возвращаем при блюре
	$('input,textarea').focus(function(){
		$(this).data('placeholder',$(this).attr('placeholder'))
		$(this).attr('placeholder','');
	});
	$('input,textarea').blur(function(){
		$(this).attr('placeholder',$(this).data('placeholder'));
	});

	$('.quarters_handler').on('click', function(){
		$('.quarters_handler').removeClass('active').eq($(this).index()).addClass('active');
		$('.quarters_items').fadeOut(0).eq($(this).index()).fadeIn(300);
		$('.quarters_about').fadeOut(0).eq($(this).index()).fadeIn(300);
	});

	$('.phoneinput').inputmask({
		mask : "+7 (999) 999-99-99"
	});


	$('.tabs_handler').on('click', function(){
		if(!$(this).hasClass('active')){
			$('.tabs_handler').removeClass('active').eq($(this).index('.tabs_handler')).addClass('active');
			if($(window).width() > 480){
				$('.tabs_item').fadeOut(0).eq($(this).index('.tabs_handler')).fadeIn(300);
			}
			else{
				$('.tabs_handler_subcontent').fadeOut(0).removeClass('active').eq($(this).index('.tabs_handler')).fadeIn(300).addClass('active')
			}
		}
	});


	// maps //
	ymaps.ready(function(){
		$('.map').each(function(){
			var th = $(this);
			var myMap = new ymaps.Map(th.data('map'), {
				center: [Number(th.data('sh')), Number(th.data('dg'))],
				zoom: Number(th.data('zoom'))
			});
			myMap.controls.remove('searchControl');
			myMap.behaviors.disable('scrollZoom');
			$('#' + th.data('map') + ' .map-metka').each(function(){
				myMap.geoObjects.add(new ymaps.Placemark([$(this).data('shirota'), $(this).data('dolgota')], {
					balloonContent: $(this).html()
				}));
			});
		});
	});

	var wow = new WOW({
    offset:       0,          // distance to the element when triggering the animation (default is 0)
    mobile:       false,       // trigger animations on mobile devices (default is true)
    live:         false,       // act on asynchronously loaded content (default is true)
    callback: function(box){
    	if($(box).hasClass('advantages_item_head--1')){
    		$('.advantages_item_head--1').animateNumber({
    			number: 300,
    			easing: 'easeInQuad',
    		}, 3000);
    	}
    	if($(box).hasClass('advantages_item_head--2')){
    		$('.advantages_item_head--2').animateNumber({
    			number: 400,
    			easing: 'easeInQuad',
    		}, 3000);
    	}
    	if($(box).hasClass('advantages_item_head--3')){
    		$('.advantages_item_head--3').animateNumber({
    			number: 500,
    			easing: 'easeInQuad',
    		}, 3000);
    	}
    }
  });
	wow.init();


	$('.owl-carousel').each(function(){
		var th = $(this).data('class');
		if($(this).data('loop')){
			var looped = true
		}
		else{
			var looped = false
		}
		$('.' + th).owlCarousel({
			items: $('.' + th).data('items'),
			smartSpeed: 700,
			loop: looped,
			lazyLoad: false,
			lazyContent: true,
			responsive: {
				0   : {items: 1},
				480 : {items: 2},
				768 : {items: 3}
			}
		});
	});

	$(".carousel-prev").click(function(e){
		var th = $(this).data('class');
		$('.' + th).trigger("prev.owl.carousel");
	});
	$(".carousel-next").click(function(e){
		var th = $(this).data('class');
		$('.' + th).trigger("next.owl.carousel");
	});


	$('.header_mob_menu-toggler').on('click', function(){
		$(this).toggleClass('active');
		$('.header_mob_menu').fadeToggle(300)
	});

	$('.header_contact').on('click', function(){
		$('body, html').animate({
			scrollTop : $('.map_block').offset().top
		}, 700)
	});

	var content = $('.managers_items').html();

	if($('.managers_item_wrapper').size() > 5){
		function carousel(){
			$('.managers_items').html('').append('<i class="carousel-prev" style="display: block"></i><i class="carousel-next" style="display: block"></i><div class="owl-carousel"></div>');
			$('.managers_items .owl-carousel').append(content);
			$('.managers_items .owl-carousel').owlCarousel({
				items: 3,
				smartSpeed: 700,
				loop: true,
				center: true,
				responsive: {
					970 : {items: 3},
					768 : {items: 2},
					480 : {items: 3},
					400 : {items: 2, center: false},
					0 : {items: 2, center: false}
				}
			});
		}
		carousel();
		$(window).on('resize', function(){
			carousel()
		});
	}

	$(document).on('click', '.managers_items .carousel-prev', function(){
		$('.managers_items .owl-carousel').trigger("prev.owl.carousel");
	});
	$(document).on('click', '.managers_items .carousel-next', function(){
		$('.managers_items .owl-carousel').trigger("next.owl.carousel");
	});


	setInterval(function(){
		setTimer();
	}, 60000);

	function setTimer(){
		var day = Number($('.lead_timer_items').data('time').slice(0, 2));
		var month = Number($('.lead_timer_items').data('time').slice(3, 5)) - 1;
		var year = Number($('.lead_timer_items').data('time').slice(6, 10));
		var dateVal = new Date(year, month, day);
		var stayDays = ( dateVal - ( new Date() ) ) / ( 1000 * 60 * 60 * 24);
		var stayHours = ( stayDays - Math.floor(stayDays) ) * 24;
		var stayMinutes = ( stayHours - Math.floor(stayHours) ) * 60;
		$('#days').text(Math.ceil( stayDays ));
		$('#hours').text(Math.ceil( stayHours ));
		$('#minutes').text(Math.ceil( stayMinutes ));
	}
	setTimer();


});